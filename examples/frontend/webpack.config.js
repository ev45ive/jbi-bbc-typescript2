const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    mode: 'development', //  https://webpack.js.org/configuration/mode/
    devtool: 'inline-source-map', // https://webpack.js.org/configuration/devtool/
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },  
    plugins: [new HtmlWebpackPlugin({
        template:'./public/index.html',
    })],
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    }
};