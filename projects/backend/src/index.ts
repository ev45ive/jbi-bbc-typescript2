import express, { ErrorRequestHandler } from 'express'
import { join } from 'path'
import { HttpError } from './common'
import { booksRouter } from './routes/books'

import { session } from 'express-session';
session({
    option: 'dsa'
}).toLocaleLowerCase()

console.log(process.cwd())
console.log(join(__dirname, '../public/favicon.ico'))

const app = express()
app.use(express.json({}))
app.use(express.static('/public'))

app.use('/books', booksRouter)

app.get('/', (req, res) => {
    res.send('Hello Books API')
})

const errorHandler: ErrorRequestHandler = (error: Error, req, res, next) => {
    if (error instanceof HttpError) {
        res.status(error.status)
        // error.handle(res)
    } else {
        res.status(500)
    }
    res.json({ error: { message: error?.message } })
}
app.use(errorHandler)

const PORT = Number(process.env.PORT || 3000)

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
})