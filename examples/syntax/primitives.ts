
let isThisBookOkay: boolean = true;
isThisBookOkay = true
isThisBookOkay = false

Number(123)
Number('123')
new Number('123') // not a number - object!

const num: number = Number(123)

const result2: number = 123 + 0xbeef + 0b1111 + 0o700;
// 49465


// const literalType : 'someoption' = 'someoption'
const firstName: string = "Mateusz";
const lastName: string = "Kulesza";
const hello: string = `Hello,
${firstName} ${lastName}!`;
// Hello,\nMateusz Kulesza!

let something: string | number | undefined = 123
something = '123'

function getValue(key: string): any { }

// OK, return value of 'getValue' is not checked
const str: string = getValue("myString");

// Any propagates down to members:
let looselyTyped: any = {};
let d = looselyTyped.a.b.c.d;
// ^ = let d: any

let maybe: string | number | undefined = 123 as any // comes from server API

// maybe.toString()

// maybe = '123' // never - some branches would be impossible

if (typeof maybe === 'string') {
    maybe.toUpperCase()
} else if (maybe !== undefined) {
    maybe.toExponential(2)
} else {
    maybe
}

const maybeName: string | undefined = '123' as string | undefined;

const alawysName = maybeName ? maybeName.toUpperCase() : 'NoName'

// Function returning never must not have a reachable end point
function error(message: string): never {
    throw new Error(message);
    // return true
}

// Inferred return type is never
function fail() {
    error("Something failed");
    return true
}

// Function returning never must not have a reachable end point
function infiniteLoop(): never { while (true) { } }


function create(o: object | null): void { };
// OK
create(new Object());
create(new Array());
create({ prop: 0 });
create(null);
// create(42);

// Way 1: Type[]
let list: number[] = [1, 2, 3];

// The second way uses a generic array type, Array<elemType>:

// Way 2: Array<Type>

let list2: Array<number | string> = [1, '3', 2]; // fetch from somewhere...
let lx = list2[1]

// This is NOT and Array! This is a tuple!
let notAList: [number, string] = [1, '2']
let aTuple = [1, '2'] as const


const convertMe: string | undefined = 123 as unknown as any