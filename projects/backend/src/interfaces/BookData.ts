export interface BookData {
    kind: "books#volume"; // string literal type
    id: string;
    volumeInfo: {
        title: string;
        subtitle: string;
        authors: string[];
        description: string;
        pageCount: number;
        categories: string[];
        averageRating?: number;
        ratingsCount?: number;
    };
}

export type CreateBookPayload = Partial<BookData['volumeInfo']>
export type UpdateBookPayload = Partial<BookData['volumeInfo']>

const c: CreateBookPayload = {} 