// const item = { name: 'item' as const, value: 42 } 
const item: {
    name: string;
    value: number;
} = { name: 'item', value: 42 }

function printItem(item: { name: string, value: number | string }) {
    return item.name + ' : ' + item.value
}

// Or can be aliased for reuse
type Item = { name: string, value: number };

const item2: Item = { name: 'item', value: 42 }

function printItem2(item: Item) {
    return item.name + ' : ' + item.value
}

interface Item2 { name: string, value: number }

interface ItemA { name: string }
interface ItemB { name: string }

interface Item42 {
    name: string | number;
    value: number;
    more?: string
}

const item3: Item42 = { name: 'item', value: 42/* , more: 'true'  */ }

const alwaysString = item3.more ? item3.more.toUpperCase() : 'No value'
const maybeString = item3.more && item3.more.toUpperCase() || 'No value'
const maybeString2 = item3.more?.toUpperCase()
const alwaysString2 = item3.more ?? 'No Value'

const trustMeIamAString = item3.more!.toUpperCase()

if ('string' === typeof item3.more) {
    item3.more
} else {
    item3.more
}

type maybe = string | number
type merged = Item42 & { extra: string }

// base.ts
interface ItemMerged { name: string }

// pluginA.ts
interface ItemMerged { pluginData: string }

const data: ItemMerged = {
    name: '123', pluginData: 'dawd'
}

interface Entity {
    id: string
    type: string
    name: string
}

interface Track extends Entity {
    type: 'track';
    duration: number;
}

interface PlaylistImage {
    url: string;
}

// interface Playlist extends IAmNamed, IHaveTracks {
interface Playlist extends Entity {
    type: 'playlist'
    images: PlaylistImage[]
    public: boolean
}

interface Playlist {
    tracks?: Track[]
}

type StringNum = string | number
type PlayTrack = Playlist & Track

// interface PlaylistWithTracks extends Playlist {
//     tracks: Track[];
// }

// type Playlist2 = Entity & Playlist
type PlaylistWithCache = Playlist & { updatedAt: Date, isFresh: boolean }

const playlistFromCache: PlaylistWithCache = {} as PlaylistWithCache

let playlist: Playlist = playlistFromCache;

const somePlaylist: Playlist = {
    id: "53Y8wT46QIMz5H4WQ8O22c",
    type: 'playlist',
    images: [{ url: 'image url' }],
    name: "Wizzlers Big Playlist",
    public: true,
    tracks: [
        {
            id: "123", type: 'track', name: "Some Track", duration: 300
        }
    ]
}

if (somePlaylist.tracks) somePlaylist.tracks.length
const numbr = somePlaylist.tracks ? somePlaylist.tracks.length : 0
somePlaylist.tracks?.length ?? 0
somePlaylist.tracks && somePlaylist.tracks.length
somePlaylist.tracks?.length
somePlaylist.tracks!.length

interface Vector { readonly x: number, readonly y: number, readonly length: number }
interface Point { readonly x: number, readonly y: number }

let v: Vector = { x: 213, y: 2345, length: 1234 }
let p: Point = { x: 213, y: 2345 }

// p = v // OK!
// v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.


type NumberOrString = number | string
const x: NumberOrString = 123 as any;
if ('string' === typeof x) {
    x.toUpperCase()
} else {
    x.toExponential()
}

type SearchResult = Playlist | Track
const res: SearchResult = {} as any;

if (res.type === 'playlist') {
    res.images
    res.tracks?.find
} else if (res.type === 'track') {
    res.duration
} else {
    res
}

type pid = Playlist['id']
type ptracks = Playlist['tracks']

interface PlaylistCache {
    [playlist_id: string]: Playlist | undefined;
}
const pcache: PlaylistCache = {
    '123': somePlaylist,
    '234': somePlaylist,
    [1 + 2]: somePlaylist,
    [playlist.id + 'pancakes']: somePlaylist
}
pcache['1234']?.name


export { }

type RequestCredentials =
    | "omit"
    | "same-origin"
    | "include";

let cred: RequestCredentials;
cred = "omit"; // OK
// cred = "inclade"; // Error!

type Headers = "Accept" | "Authorization" /* … */;
type ExtraHeaders = "X-Proxy" /* … */;
type MergedHeaders = Headers | ExtraHeaders

type RequestConfig = {
    [key in MergedHeaders]: string;
};
const request: RequestConfig = {
    Accept: '', Authorization: '', "X-Proxy": ''
}