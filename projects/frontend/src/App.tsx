import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import { BookSearch } from './pages/BookSearch';

function App() {
  return (
    <div className="App">
      {/* .container>.row>.col */}
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>BookSearch</h1>
            <BookSearch/>
          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
