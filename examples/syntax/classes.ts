
// interface Person {
//     name: string
// }

// class SpecialPerson implements Person {
//     name: string
//     sayHello(): void {
//         throw new Error("Method not implemented.")
//     }
// }

interface IHaveAnId { id: string }
interface IHaveAName { readonly name: string }

class Person implements IHaveAnId, IHaveAName {
    // readonly name!: string
    // private name!: string
    // protected name!: string
    // public name!: string

    // name?: string
    // [k: string]: string | Function
    // name = 'Default'.toUpperCase()

    id: string = Date.now().toString()

    constructor(readonly name: string = 'No Name') {
        // this.name = name
        // this.name = undefined
    }

    sayHello(greeting?: string) {
        return 'I am ' + this.name
    }
}

class Employee extends Person {

    constructor(name?: string) {
        // parent constructor call is required!
        super(name)
    }

    sayHello() {
        // optionally can call overriden methods
        return super.sayHello() + ' and i love my work!'
        // return 'asd' as const
    }
}


class Singleton {
    private id: string = ''
    private constructor() { }

    static instance: Singleton

    static create() {
        Singleton.instance = new Singleton()
        Singleton.instance.id = Date.now().toString()
        return Singleton.instance
    }
    static loadFromJSON(json: any) {
        Singleton.instance = new Singleton()
        Singleton.instance.id = json.id
        return Singleton.instance
    }
    static saveToJson(singleton: Singleton) {
        return { id: singleton.id }
    }
}


// const s = new Singleton() // error
const s = Singleton.create() // OK
// s.id // error
// Singleton.saveToJson(s).id // OK