export { }

type Item = {
    name: string;
    value: number;
};

const item: Item = { name: 'item', value: 42 }

let myAdd1: (baseValue: number, increment: number) => number = function (x: number, y: number): number {
    return x + y;
};

// type AddFunctionSignature = (baseValue: number | string, increment: number | string) => number | string; // 
type AddFunctionSignature = (baseValue: number, increment: number) => number;

let myAdd2: AddFunctionSignature = (x, y) => { return x + y; };

interface AddFunctionInterface {
    (baseValue: number, increment: number): number
    (baseValue: string, concat: string): string
    (baseValue: any, concat: any): any
    // (baseValue: Array<string>, concat: Array<string>): string
}

// function myAdd3(baseValue: number, increment: number): number
// function myAdd3(baseValue: string, concat: string): string
// function myAdd3(x: any, y: any) {
//     if (typeof x == 'number') {
//         return x + y
//     }
//     if (typeof x == 'string') {
//         return x + y
//     }
// };


function myAdd3(baseValue: number, increment: number): number
function myAdd3(baseValue: string, concat: string): string
function myAdd3(x: string | number, y: string | number) {
    if (typeof x == 'number' && typeof y == 'number') {
        return x + y
    }
    if (typeof x == 'string' && y == 'string') {
        return x + y
    }
};

// const resNum = myAdd3('123', 123)
const resNum = myAdd3(123, 123)
const resStr = myAdd3('123', '123')
// myAdd3()
// const resStr2 = myAdd3([], ['123'])

interface SearchFunc {
    (source: string, subString: string): boolean;
    (source: string, subString: string, startPos: number): boolean; // overload
}
const mySearch: SearchFunc = (heap: string, needle: string, start = 0) => heap.includes(needle, start);
mySearch('test', 'tes', 2)

let mySearchCallback: SearchFunc | null = null
mySearchCallback = (x: string, y: string, start?: number) => true;

[1, 2, 3].filter((value: number/* , index: number, array: number[] */) => true)


const obj1 = { a: 1, b: 2, items: [1, 2, 3] }
const obj2 = { /*  */b: 3, c: 4, items: [5] }
const merged = { ...obj1, ...obj2 } // {a:1, b:3, c:4, items:[5] }

function sum(initialValue = 0, ...rest: number[]): number {
    return rest.reduce((sum, x) => sum + x, initialValue)
}
sum(1, 2, 3, 4)
sum(...[1, 2, 3, 4])

function only2(...params: [string, number]): [string, number] {
    params[0].toUpperCase()
    params[1].toFixed()
    return ['string', 123]
}
only2('123', 123)

interface Elem {
    name: 'elem'
    onclick: (this: Elem, event: Event) => void
}

const element: Elem = {
    name: 'elem',
    onclick(event) {
        this.name == 'elem'
    }
}

document.createElement('div')