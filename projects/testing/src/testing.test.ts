import { expect } from "chai"
import sinon from "sinon"

describe('Testing tests', () => {

    it('true is truthy', () => {
        // expect(true).toBeTruthy()
        expect(true).to.be.true
    })

    it('async test', (done) => {
        setTimeout(() => {
            expect(true).to.be.true
            done()
        }, 0)
    })

    it('async test', () => {
        const p = new Promise((resolve) => setTimeout(() => {
            expect(true).to.be.true
            resolve('result')
            // throw new Error('failed promise')
        }, 0))

        return p
    })

    it('Test Mock', () => {

        function hello(name: string, cb: (resp: string) => string) {
            const res = cb('Hello ' + name)
        }
        const spy = sinon.spy((resp: string) => '')

        hello('Alice', spy)

        expect(spy).to.have.been.calledWith('Hello Alice')
    })

    it('Tests mocks', () => {
        class TestingService {
            constructor() { }
            someTestMethod(x: string) { return x }
        }

        // sinon.spy
        // sinon.mock
        // const stub = sinon.stub(new TestingService, 'someMethod')
        // expect(stub('test')).to.eq('test')

        const stub = sinon.createStubInstance(TestingService, {})
        stub.someTestMethod.returns('awesome')

        stub.someTestMethod('test')

        expect(stub.someTestMethod).to.have.been.calledWith('test').and.returned('awesome')
    })

    class ServerService { getEpisode(id: string) { return { name: 'episode', id } } }

    let service: ServerService
    // let getEpisodeMock: sinon.SinonStub<[id: number], { name: string; id: number; }>
    let getEpisodeMock: sinon.SinonStub<Parameters<ServerService['getEpisode']>, ReturnType<ServerService['getEpisode']>>

    beforeEach(() => {
        service = new ServerService()
        const sandbox = sinon.createSandbox()
        getEpisodeMock = sandbox.stub(service, 'getEpisode')
    })

    it('test mocks', () => {
        getEpisodeMock('test')
        expect(getEpisodeMock).to.have.been.calledWith('test')
    })

    /* Immutable, functional + BONUS = Types Are Inferred!  */

    const setup = () => {
        const service = new ServerService()
        const sandbox = sinon.createSandbox()
        const getEpisodeMock = sandbox.stub(service, 'getEpisode')

        return { service, getEpisodeMock }
    }


    it('test mocks', () => {
        const { getEpisodeMock, service } = setup()
        getEpisodeMock('test')
        expect(getEpisodeMock).to.have.been.calledWith('test')
    })


})
