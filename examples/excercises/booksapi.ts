// https://developers.google.com/books/docs/v1/reference/volumes

export {}

interface BookData {
    kind: "books#volume", // string literal type
    id: string,
    volumeInfo: {
        title: string,
        subtitle: string,
        authors: string[],
        description: string,
        pageCount: number,
        categories: string[],
        averageRating?: number,
        ratingsCount?: number,
    }
}

interface CreateBookPayload {
    title?: string,
    authors?: string[],
}

interface UpdateBookPayload {
    title?: string,
    authors?: string[],
}

const books: BookData[] = [{
    id: "123", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['Programming'], description: '', pageCount: 657, subtitle: '', title: 'TypeScript'
    }
}, {
    id: "234", kind: 'books#volume', volumeInfo: {
        authors: ['Bob'], categories: ['Programming'], description: '', pageCount: 345, subtitle: '', title: 'JavaScript'
    }
}, {
    id: "345", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['SciFi'], description: '', pageCount: 234, subtitle: '', title: 'JavaScript and React'
    }
}]

// const displayBookTagline = (book: BookData) => {
//     const { volumeInfo: { title, authors: [author], categories: [category], pageCount: pages } } = book
//     return `${title} - ${author} [${category}] (${pages})`
// }
const displayBookTagline = ({
    volumeInfo: {
        title, authors: [author], categories: [category], pageCount: pages
    }
}: BookData) => `${title} - ${author} [${category}] (${pages}p)`;


const findAllBooks = () => { return books }
const findBookById = (id: BookData['id']) => { return books.find(b => b.id === id) }

const findBooksByTitle = (title: BookData['volumeInfo']['title']) => {
    return books.find(b => b.volumeInfo.title === title)
}
const findBooksByAuthor = (author: string) => {
    return books.find(b => b.volumeInfo.authors.includes(author))
}
const findBooksByCategory = (category: string) => {
    return books.find(b => b.volumeInfo.categories.includes(category))
}

type Criteria =
    | { title: string }
    | { category: string }
    | { author: string }
    | { id: string }

function queryBooks(criteria: { id: string }): BookData | undefined
function queryBooks(criteria: { title: string }): BookData[] | undefined
function queryBooks(criteria: { category: string }): BookData[] | undefined
function queryBooks(criteria: { author: string }): BookData[] | undefined
function queryBooks(criteria: Criteria): BookData | BookData[] | undefined {
    if ('title' in criteria) { return findBooksByTitle(criteria.title) }
    else if ('category' in criteria) { return findBooksByCategory(criteria.category) }
    else if ('author' in criteria) { return findBooksByTitle(criteria.author) }
    else if ('id' in criteria) { return findBooksByTitle(criteria.id) }
    else {
        // https://www.typescriptlang.org/docs/handbook/unions-and-intersections.html#union-exhaustiveness-checking
        // const _never: never = criteria
        checkExhaustiveness(criteria)
    }
}
function checkExhaustiveness(_never: never): never {
    throw new Error('Unexpected case ' + _never)
}

queryBooks({ id: '123' })


const createBook = (payload: CreateBookPayload): BookData => {

    const book: BookData = {
        id: Date.now().toString(),
        kind: 'books#volume',
        volumeInfo: {
            description: '',
            title: '',
            categories: [],
            authors: [],
            pageCount: 0,
            subtitle: '',
            averageRating: 0,
            ratingsCount: 0,
            ...payload
        }
    }
    return book
}
const updateBook = (id: BookData['id'], payload: UpdateBookPayload): BookData | Error => {
    const book = findBookById(id)

    if (!book) { return new Error('Book not found!'); }

    const updated: BookData = {
        ...book,
        volumeInfo: {
            ...book.volumeInfo,
            ...payload
        }
    }
    return updated
}
const deleteBook = (id: BookData['id']) => {
    return books.filter(b => b.id !== id)
}

findAllBooks().map(displayBookTagline).forEach(console.log)

function scenario() {
    const book = updateBook('132', { authors: [], title: '' })
    if ((book instanceof Error)) {
        return;
    }
    return book
}


type keys = 'title' | 'pageCount' | 'averageRating'

enum KEYS {
    title = 'title',
    pageCount = 'pageCount',
    // averageRating = 'averageRating',
    // DEFAULT = KEYS.title
}

enum DIR {
    ASC = 1, DESC = -1, DEFAULT = DIR.ASC
}

const bookSorter = {
    dir: DIR.DEFAULT,
    key: KEYS.title,
    getComparator() {
        return function (this: typeof bookSorter, a: BookData, b: BookData) {
            console.log(this);

            if (this.dir === DIR.ASC) {
                return a.volumeInfo[this.key] > b.volumeInfo[this.key] ? -1 : 1
            } else {
                return a.volumeInfo[this.key] > b.volumeInfo[this.key] ? 1 : -1
            }
        }.bind(this)
    }
}
type BookSorterType = typeof bookSorter
type BookSorterType2 = {
    dir: DIR;
    key: KEYS;
    getComparator(): (a: BookData, b: BookData) => 1 | -1;
}

books.sort(bookSorter.getComparator())