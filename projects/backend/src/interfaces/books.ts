import { BookData, CreateBookPayload, UpdateBookPayload } from "./BookData";


interface BookIdParam {
    id: string
}

export interface BookQueryCriteria {
    title: string
}

export namespace GetBooks {
    export type Params = void
    export type Response = BookData[]
    export type Query = BookQueryCriteria
    export type RequestBody = void
}

export namespace GetBook {
    export type Params = BookIdParam
    export type Response = BookData
    export type Query = BookQueryCriteria
}

export namespace CreateBook {
    export type Params = BookIdParam
    export type Response = BookData
    export type RequestBody = CreateBookPayload
}

export namespace UpdateBook {
    export type Params = BookIdParam
    export type Response = BookData
    export type RequestBody = UpdateBookPayload
}