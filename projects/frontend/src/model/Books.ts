
export interface BookData {
    kind: "books#volume", // string literal type
    id: string,
    volumeInfo: {
        title: string,
        subtitle: string,
        authors: string[],
        description: string,
        pageCount: number,
        categories: string[],
        averageRating?: number,
        ratingsCount?: number,
        "imageLinks"?: {
            "smallThumbnail": string,
            "thumbnail"?: string,
            "small": string,
            "medium": string,
            "large": string,
            "extraLarge": string
        },
    }
}

export interface BooksResponse {
    "kind": "books#volumes",
    "items": BookData[],
    "totalItems": number
}

export interface BooksAPIErrorResponse {
    "error": {
        "code": string,
        "message": string
        /* "errors": [
            {
                "message": "Missing query.",
                "domain": "global",
                "reason": "queryRequired",
                "location": "q",
                "locationType": "parameter"
            }
        ] */
    }
}
