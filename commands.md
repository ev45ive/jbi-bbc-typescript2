
## Install TypeScript
npm install --global typescript
tsc --version
Version 4.2.4

tsc hello.ts hello2.ts 
tsc hello2.ts --target es2015 --outFile output.js
tsc hello.ts --strict

## TS Configuration
npm init -y
tsc --init --strict --target es2015 

## NodeJS TsConfig
npm i --save-dev @types/node

## Webback ts-loader
npm i webpack webpack-cli ts-loader typescript 

## Express JS
npm i --save express
npm i --save-dev @types/express

## TS tools:
tsc --watch
npm i -g nodemon ts-node ts-node-dev

tsc --watch
node --inspect-brk --enable-source-maps ./examples/backend/dist/index.js 

<!-- https://github.com/TypeStrong/ts-node#configuration-options -->
TS_NODE_PROJECT=./tsconfig.json nodemon -r "ts-node/register" --inspect-brk --  excercises/booksapi.ts

## Unions, Pattern matching

If anyone is interested in Functional Programming in JavaScript, I would definitely recommend this book, it’s free -> https://github.com/MostlyAdequate/mostly-adequate-guide

http://learnyouahaskell.com/chapters

https://www.typescriptlang.org/docs/handbook/unions-and-intersections.html#union-exhaustiveness-checking

This could potentially make itself into the language -> https://github.com/tc39/proposal-pattern-matching

https://github.com/gvergnaud/ts-pattern#__-wildcard


## React + Typescript
npx create-react-app frontend --template typescript

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets