import React, { useState } from 'react'

interface Props {
    onSearch(query: string): void
}

export const SearchBox = ({ onSearch }: Props) => {
    const [query, setQuery] = useState<string>('')

    const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setQuery(e.currentTarget.value)
    }

    // if (query == null) { return }
    // query
    return (
        <div>

            <div className="input-group mb-3">
                <input type="text" className="form-control" placeholder="Search" value={query} onChange={changeHandler} />
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary" type="button" onClick={() => onSearch(query)}>Search</button>
                </div>
            </div>

        </div>
    )
}
