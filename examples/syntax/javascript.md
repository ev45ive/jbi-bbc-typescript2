
## Closures
```js
var global = 'global';

debugger

function A(name){
    var outer = 'outer';
    debugger;
    B();
    
    function B(){
        var inner = 'inner'
        debugger;
        console.log(global, outer,inner, name)
    }

}
A('param')
```

```js
function makePerson(name){
    var obj = { 
        name: name,
        sayHello: function(){ return 'Hi I am ' + obj.name }
    }

    return obj;
}
alice = makePerson('alice')
console.log(alice.sayHello())
alice.name = 'Bob'
console.log(alice.sayHello())
```

```js
function sayHello(self){ return 'Hi I am ' + self.name }

function makePerson(self, name){
    self.name  = name,
    self.sayHello = sayHello

    return self;
}
// alice = makePerson('alice')

alice = Object.create({})

makePerson(alice,'Alice')
sayHello(alice,'')
```

```js
function sayHello(self){ return 'Hi I am ' + self.name }

function makePerson(self, name){
    self.name  = name,
    self.sayHello = sayHello

    return self;
}
// alice = makePerson('alice')

alice = { } 
makePerson(alice,'Alice')

sayHello(alice,'')

```
```js
function sayHello(){ return 'Hi I am ' + this.name }

function Person(name){
    this.name  = name,
    this.sayHello = sayHello
}
// alice = makePerson('alice')

// alice = Person.call({},'Alice')
alice = new Person('Alice')
sayHello(alice)

```

```ts
function sayHello(){ return 'Hi I am ' + this.name }

function Person(name){
    this.name  = name,
    this.sayHello = sayHello
}
// alice = makePerson('alice')

// alice = Person.call({},'Alice')
alice = new Person('Alice')
bob = new Person('Bob')
alice.sayHello === bob.sayHello
```

```js
function Person(name){
    this.name = name
}

Person.name === 'Person'
Person.LEGAL_AGE = 18;
Person.isLegal(person){ return Person.LEGAL_AGE < person.age }

Person.prototype = {
    toString: function (){ return '[Person '+this.name+']' },
    sayHello: function sayHello(){ return 'Hi I am ' + this.name }
}

// alice = Person.call({},'Alice')
alice = new Person('Alice')
bob = new Person('Bob')
alice.sayHello === bob.sayHello
alice.toString()


function Employee(name, salary){
    Person.apply(this, arguments)
    // Logger.apply(this, arguments)
    // PayrollSubject.apply(this, arguments)
    // ApplyMixins(mixn1,minxin2, this, argumetns)
    this.salary = salary
}

Employee.prototype = Object.create(Person.prototype)
Object.assign(Employee.prototype, {
    toString: function (){ return '[Employee '+this.name+']' },
    doWork: function sayHello(){ return 'Hi I need my ' + this.salary }
})
tom = new Employee('Tom',2500)
console.log(tom.sayHello())
console.log(tom.toString())
console.log(tom.doWork())
// VM15374:29 Hi I am Tom
// VM15374:30 [Employee Tom]
// VM15374:31 Hi I need my 2500
undefined
tom 
// Employee {name: "Tom", salary: 2500}
// name: "Tom"
// salary: 2500
// __proto__: Array
//     doWork: ƒ sayHello()
//     toString: ƒ ()
//     __proto__: Array
//         sayHello: ƒ sayHello()
//         toString: ƒ ()
//             __proto__: Object

Person.prototype.party = function(){ return "Yeah! Party!" }
// ƒ (){ return "Yeah! Party!" }
tom.party()
"Yeah! Party!"
```

```js
class Person{
    greeting = 'Hi, I am '
    
    constructor(name){
        this.name = name;
    }

    sayHello(){
        return this.greeting + this.name 
    }
}
var alice = new Person('Alice')
console.log(alice.sayHello())
alice 
// VM16251:13 Hi, I am Alice
// Person {greeting: "Hi, I am ", name: "Alice"}
//     greeting: "Hi, I am "
//     name: "Alice"
//         __proto__: 
//         constructor: class Person
//         sayHello: ƒ sayHello()
//             __proto__: Object

alice instanceof Person 
true
```

```js
class Employee extends Person{

    constructor(name,salary){
        super(name)
        this.salary = salary
    }

    sayHello(){
        return super.sayHello() + ' I am an employee'
    }

    doWork(){ return 'I need my '+this.salary }
}
undefined
tom = new Employee('Tom',1235)
console.log(tom.sayHello())
console.log(tom.doWork())
tom 
VM17119:2 Hi, I am Tom I am an employee
VM17119:3 I need my 1235
// Employee {greeting: "Hi, I am ", name: "Tom", salary: 1235}
// greeting: "Hi, I am "
// name: "Tom"
// salary: 1235
// __proto__: Person
//     constructor: class Employee
//     doWork: ƒ doWork()
//     sayHello: ƒ sayHello()
//         __proto__:
//             constructor: class Person
//             sayHello: ƒ sayHello()
```

```js
let MyMixin = (superclass) => class extends superclass {
  foo() {
    console.log('foo from MyMixin');
  }
};

class MyClass extends MyMixin(MyBaseClass) {
  /* ... */
}
```