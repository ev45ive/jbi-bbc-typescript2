export { }

const map1: Map<string, number> = new Map([
    ['key', 123], ['key', 123], ['key', 123],
])
map1.get('123')?.toExponential()


interface Map<K, V> {
    get(key: K): V | undefined;
    has(key: K): boolean;
    set(key: K, value: V): this;
}

const arr1: Array<string> = ['string']
const arr2: Array<number> = [1, 2, 3]
const arr3: Array<Array<string>> = [
    ['X', 'O', ''],
    ['', 'X', 'X'],
    ['', '', 'O'],
]

// Instance Type vs Class Type

class Person { x: string = ''; static y() { } }

const cInst: Person = { x: '' }
const cInst2: Person = new Person()
type Person2 = typeof cInst

type PersonConstrorType = typeof Person
// const cConstr: PersonConstrorType = { y() { }, prototype: Person.prototype }


// function echo(msg: any) { return msg } 
// x = echo() // any!!!

function echo<MsgT>(msg: MsgT): MsgT {
    console.log(msg)
    return msg
}

const genericresult1 = echo<string>('123')
const genericresult2 = echo<number>(123)
const genericresult3 = echo([[123, 'string']])
const genericresult4 = echo(new Person())
const genericresult5: string = echo('123')

type AnyRole = {
    [role: string]: string | undefined
}
const roles/* : AnyRole */ = {
    'admin': 'ADMIN_ROLE',
    'user': 'USER_ROLE',
    'moderator': 'MODERATOR_ROLE',
}
// roles2 = { ...roles, 'test': 'TEST_ROLE' }

// type ROLES = 'admin' | 'user'
type ROLES = keyof typeof roles

/// ====
// function getRole(role: ROLES) {
// function getRole<T = ROLES>(role: T) {
// function getRole<T extends ROLES>(role: T) {
// function getRole<T extends keyof typeof roles>(role: T) {
// function getRole<T>(roles: T, role: keyof T) {

function getRole<T, K extends keyof T>(roles: T, role: K) {
    return roles[role]
}
getRole(roles, 'admin')

interface User { id: string, role: ROLES }
interface Admin { id: string, role: 'admin' }
interface Moderator { id: string, role: 'moderator' }

function addUser<T extends User>(user: T): T['role'] {
    return user.role
}

// const u01 = addUser({})
const u1 = addUser({} as Admin)
const u2 = addUser({} as Moderator)

function defaults<A extends object, B extends A>(override: A, defaults: B): A & B {
    return { ...defaults, ...override }
}
const res0 = defaults({ ac: false, wifi: false, minibar: false }, { ac: false, wifi: true, minibar: false })
// const res1 = defaults({ user: 'guest' /* , unknownProp:true */ },{ isEnabled: false, user: null })
// const res2 = defaults({ ac: false, wifi: false, minibar: false }, { wifi: true })

/* 
    <T>
    <T = ??>
    <T extends ??>
    <T extends keyof typeof ...>
*/


class Queue<T> {
    constructor(private data: T[] = []) { }
    push(item: T) { this.data.push(item); }
    pop(): T | undefined { return this.data.shift(); }
}
const strQ = new Queue(['123'])
strQ.push('123')

const strnumQ = new Queue(['123', 123])
strnumQ.push('123')
strnumQ.push(234)


interface Constructable<T> {
    // (...args: any[]): T
    new(...args: any[]): T;
}

class Car { }

type CarT = typeof Car;

const c1: Car = Car;
const constr1: Constructable<Car> = Car

const c2: Car = new Car();
const constr2: Constructable<typeof c2> = Car

/// ===========


interface BookData {
    kind: "books#volume", // string literal type
    id: string,
    title: string,
    subtitle: string,
    authors: string[],
    description: string,
    pageCount: number,
    categories: string[],
    averageRating?: number,
    ratingsCount?: number,
}

/* Type Mappings */

// type CreateBookPayload = BookData
// type keys = 'id' | 'authors'
// type keys =
//     | 'kind'
//     | 'id'
type keys = keyof BookData

// type PartialBook = {
//     // [akey: string]: string
//     // [akey in keys]: string
//     [akey in keys]?: BookData[akey]
// }

// type Partial<T> = {
//     // [akey: string]: string
//     // [akey in keys]: string
//     [akey in keyof T]?: T[akey]
// }

// type Pick<T extends {}, K extends keyof T> = {
//     [akey in K]: T[akey]
// }

const c23: Pick<BookData, 'id' | 'kind'> = {
    id: '',
    kind: 'books#volume'
}

// c.title = '' // Cannot assign to 'title' because it is a read-only property.

// interface CreateBookPayload {
//     title?: string,
//     subtitle?: string,
//     authors?: string[],
//     description?: string,
//     pageCount?: number,
//     categories?: string[],
//     averageRating?: number,
//     ratingsCount?: number,
// }

/* Conditional types */

// type OnlyString<T> = T extends string ? T : never
type t = BookData['title']

type OnlyStringKeys<T, V> = {
    [key in keyof T]-?: T[key] extends V ? key : never
}[keyof T]

type bookStringKeys = OnlyStringKeys<BookData, string>

type StringOnly<T> = {
    // [key in OnlyStringKeys<T, number | undefined>]: T[key]
    [key in OnlyStringKeys<T, string>]: T[key]
}

const c: Partial<StringOnly<BookData>> = {
    id: '123',

}