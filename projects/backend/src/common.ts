

export class HttpError extends Error {
    status = 500;
}

export class UnexpeectedError extends Error {
    status = 500;
    message = 'Unexpected Error'
}

export class NotFoundError extends HttpError {
    status = 401;

    constructor(readonly message: string) {
        super(message)
    }
}