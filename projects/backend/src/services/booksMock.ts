import { BookData } from "../interfaces/BookData";

export const booksMock: BookData[] = [{
    id: "123", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['Programming'], description: '', pageCount: 657, subtitle: '', title: 'TypeScript'
    }
}, {
    id: "234", kind: 'books#volume', volumeInfo: {
        authors: ['Bob'], categories: ['Programming'], description: '', pageCount: 345, subtitle: '', title: 'JavaScript'
    }
}, {
    id: "345", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['SciFi'], description: '', pageCount: 234, subtitle: '', title: 'JavaScript and React'
    }
}];
