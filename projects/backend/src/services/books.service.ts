import { NotFoundError } from "../common";
import { BookData, CreateBookPayload, UpdateBookPayload } from "../interfaces/BookData"
import { BookQueryCriteria } from "../interfaces/books"
import { booksMock } from "./booksMock"

export const findBooks = async (criteria?: BookQueryCriteria) => {
    if (!criteria) return booksMock;

    return booksMock.filter(b => {
        return b.volumeInfo.title.includes(criteria.title)
    })
}

export const getBookById = async (id: BookData['id']) => {
    const result = booksMock.find(b => b.id === id)
    if (!result) { throw new Error('Not Found') }
    return result
}

export const createBook = async (payload: CreateBookPayload) => {

    const book: BookData = {
        id: Date.now().toString(),
        kind: 'books#volume',
        volumeInfo: {
            description: '',
            title: '',
            categories: [],
            authors: [],
            pageCount: 0,
            subtitle: '',
            averageRating: 0,
            ratingsCount: 0,
            ...payload
        }
    }
    booksMock.push(book)
    return book
}

export const updateBook = async (id: BookData['id'], payload: UpdateBookPayload) => {
    const book = await getBookById(id)

    if (!book) { throw new NotFoundError('Book not found!'); }

    const updated: BookData = {
        ...book,
        volumeInfo: {
            ...book.volumeInfo,
            ...payload
        }
    }
    return updated
}

export const deleteBook = async (id: BookData['id']) => {
    const index = booksMock.findIndex(b => b.id !== id)
    if (index !== -1) {
        booksMock.splice(index, 1)
    }
}
