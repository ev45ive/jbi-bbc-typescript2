export { }

// https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
interface MediaItem<T> {
    id: string,
    update(data: ItemUpdateDTO<T>): void
    /* , dateCreated: Date */
}

interface Book extends MediaItem<Book> { title: string }
interface Album extends MediaItem<Album> { }
interface Movie extends MediaItem<Movie> { }

interface Criteria<T> {
    isMatch(this: Criteria<T>, item: T): boolean
}

interface DataMapper<T, ReadDto, WriteDto = ReadDto> {
    toJSON(item: T): WriteDto
    fromJSON(data: ReadDto): T
}
interface ItemUpdateDTO<T> { }
interface ItemCreateDTO<T> { }

interface GenericRepository<T extends MediaItem<T>, Tid = T['id']> {
    // constructor(items: T[]): void
    getById(id: Tid): T | undefined
    findOne(criteria: Criteria<T>): T | undefined
    findMany(criteria: Criteria<T>): T[]

    create(data: ItemCreateDTO<T>): T
    update(id: Tid, data: ItemUpdateDTO<T>): T
    delete(id: Tid): void
}

class BookCriteria implements Criteria<Book>{
    constructor(readonly title = ''/* bookRentalService */) { }

    isMatch(item: Book): boolean {
        /* this.bookRentalService.isRented(item) */
        return item.title.includes(this.title)
    }
}

class GenericRepo<T extends MediaItem<T>, ReadDto, WriteDto = ReadDto> implements GenericRepository<T> {

    constructor(
        private mapper: DataMapper<T, ReadDto, ReadDto>,
        private items: T[] = []) { }

    getById(id: T["id"]): T | undefined {
        return this.items.find(item => item.id === id)
    }

    findOne(criteria: Criteria<T>): T | undefined {
        return this.items.find(criteria.isMatch)
    }

    findMany(criteria: Criteria<T>): T[] {
        return this.items.filter(criteria.isMatch)
    }

    create(data: ReadDto): T {
        const item = this.mapper.fromJSON(data)
        this.items.push(item)
        return item
    }

    update(id: T['id'], data: ReadDto): T {
        const item = this.getById(id)
        if (!item) { throw new Error('Item not found!') }
        const updated = this.mapper.fromJSON(data)
        this.items = this.items.map(item => item.id === id ? updated : item)
        return item
    }

    delete(id: T["id"]): void {
        this.items = this.items.filter(item => item.id !== id)
    }

}


interface CreateBookPayload {
    title?: string,
    authors?: string[],
}

interface UpdateBookPayload {
    title?: string,
    authors?: string[],
}

class BookRepository extends GenericRepo<Book, CreateBookPayload, UpdateBookPayload>{
    findRecent() { }
}

const books = new BookRepository({
    fromJSON(data) { return { id: 123 } as unknown as Book },
    toJSON(item: Book) { return { id: 123 } as CreateBookPayload }
})
// books.findRecent()
const book = books.getById('123')