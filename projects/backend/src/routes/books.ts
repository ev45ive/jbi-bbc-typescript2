// import { Router } from "express";
import Router from "express-promise-router";
import { CreateBook, GetBook, GetBooks, UpdateBook } from "../interfaces/books";
import { findBooks, getBookById, updateBook } from "../services/books.service";
import multer from 'multer'
export const booksRouter = Router()

booksRouter.get<
    GetBooks.Params,
    GetBooks.Response,
    void,
    GetBooks.Query
>('/', async (req, res) => {
    // findBooks().then(data => res.send(data))

    const books = await findBooks(req.query)
    // res.send(new PagingObject(books, total, page))
    // GetBooks.Response<T> = PagingObject<T>
    // pagingObject.toJSON()
    res.json(books)
})

booksRouter.get<
    GetBook.Params,
    GetBook.Response | Error>('/:id', async (req, res, next) => {
        const book = await getBookById(req.params.id)
        res.send(book)
    })

booksRouter.post<
    CreateBook.Params,
    CreateBook.Response,
    CreateBook.RequestBody>('/', async (req, res) => {

    })

booksRouter.put<
    UpdateBook.Params,
    UpdateBook.Response,
    UpdateBook.RequestBody>('/:id', async (req, res) => {

        const result = await updateBook(req.params.id, req.body)
        res.send(result)
    })


var upload = multer({ dest: 'uploads/' })

booksRouter.post('/:id/upload', upload.single('avatar'), (req, res) => {
    req.file.filename
    // req.files
})


/* Declaration merging */

declare global {
    interface A { a: string }
}


declare global {
    interface A { extraStuff: string }
    function test(a: number, b: number): number;
}

const a: A = { a: 'test', extraStuff: 'daw' }

// test(1, 2) // doesnt exist in JS, TS just believes its does!

// function test(a, b) { return a + b }