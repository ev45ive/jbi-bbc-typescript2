export { }

interface BookData {
    kind: "books#volume", // string literal type
    id: string,
    volumeInfo: {
        title: string,
        subtitle: string,
        authors: string[],
        description: string,
        pageCount: number,
        categories: string[],
        averageRating?: number,
        ratingsCount?: number,
    }
}

const books: BookData[] = [{
    id: "123", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['Programming'], description: '', pageCount: 657, subtitle: '', title: 'TypeScript'
    }
}, {
    id: "234", kind: 'books#volume', volumeInfo: {
        authors: ['Bob'], categories: ['Programming'], description: '', pageCount: 345, subtitle: '', title: 'JavaScript'
    }
}, {
    id: "345", kind: 'books#volume', volumeInfo: {
        authors: ['Alice'], categories: ['SciFi'], description: '', pageCount: 234, subtitle: '', title: 'JavaScript and React'
    }
}]



interface CreateBookPayload {
    title?: string,
    subtitle?: string,
    authors?: string[],
    description?: string,
    pageCount?: number,
    categories?: string[],
    averageRating?: number,
    ratingsCount?: number,
}

interface UpdateBookPayload {
    title?: string,
    subtitle?: string,
    authors?: string[],
    description?: string,
    pageCount?: number,
    categories?: string[],
    averageRating?: number,
    ratingsCount?: number,
}

interface BookReadRepository {
    findOne(criteria: Criteria): Book | undefined
    findMany(criteria: Criteria): Book[]
    getById(id: Book['id']): Book | undefined
}
interface BookWriteRepository {
    create(payload: CreateBookPayload): Book
    update(id: Book['id'], payload: UpdateBookPayload): Book
    delete(id: Book['id']): void
}
// interface BookRepository extends BookReadRepository, BookWriteRepository{}

class Book {
    readonly kind = "books#volume"

    private _title: string = ''

    public get title(): string {
        return this._title
    }
    public set title(value: string) {
        this._title = value
    }

    private _subtitle: string = ''
    public get subtitle(): string {
        return this._subtitle
    }
    public set subtitle(value: string) {
        this._subtitle = value
    }
    description: string = ''
    private pageCount: number = 0
    private authors = new Set<string>([])
    private categories = new Set<string>([])
    private averageRating: number = 0
    private ratingsCount: number = 0

    constructor(readonly id: string) { }

    addToCategory(category: string) { this.categories.add(category) }
    getCategories() { return Array.from(this.categories.values()) }

    addAuthor(author: string) { this.categories.add(author) }
    getAuthors() { return Array.from(this.authors.values()) }

    getRating() { return `${this.averageRating.toFixed(2)} / 5 (${this.ratingsCount})` }

    rateBook(rating: number) {
        this.ratingsCount++
        this.averageRating = (this.averageRating + rating) / 2
    }

    toJSON(): BookData {
        return {
            id: this.id,
            kind: this.kind,
            volumeInfo: {
                title: this.title,
                subtitle: this.subtitle,
                description: this.description,
                pageCount: this.pageCount,
                authors: this.getAuthors(),
                categories: this.getCategories(),
                averageRating: this.averageRating,
                ratingsCount: this.ratingsCount,
            }
        }
    }

    static create(payload: CreateBookPayload) {
        const book = new Book(Date.now().toString())
        book.title = payload.title ?? ''
        payload.authors?.forEach(a => book.addAuthor(a))
        return book
    }

    update(payload: UpdateBookPayload) {
        this.title = payload.title ?? ''
        payload.authors?.forEach(a => this.addAuthor(a))
        return this;
    }

    static fromJSON(data: BookData) {
        if (data.kind !== 'books#volume') { throw 'Invalid Book Data JSON' };

        const book = new Book(data.id)
        // inconsistent book for a moment here!
        // book.readFromJSON(data)
        book.title = data.volumeInfo.title;
        book.subtitle = data.volumeInfo.subtitle;
        book.authors = new Set(data.volumeInfo.authors)
        book.categories = new Set(data.volumeInfo.categories)
        book.description = data.volumeInfo.description;
        book.pageCount = data.volumeInfo.pageCount;
        book.averageRating = data.volumeInfo.averageRating || 0
        book.ratingsCount = data.volumeInfo.ratingsCount || 0
        return book;
    }
}
type Criteria = | { title: string }

interface BookDataMapper {
    serialize(book: Book): BookData
    unserialize(book: BookData): Book
}

class BookRepository implements BookReadRepository, BookWriteRepository {

    // adapterConverter: BookDataMapper

    constructor(private books: BookData[]) { }

    delete(id: string): void {
        const index = this.books.findIndex(b => b.id === id)
        if (index !== -1) {
            this.books.splice(index, 1)
        }
    }

    findOne(criteria: Criteria) {
        const data = this.books.find(b => b.volumeInfo.title == criteria.title)
        return data ? Book.fromJSON(data) : undefined
    }

    findMany(criteria: Criteria) {
        return this.books.filter(b => b.volumeInfo.title == criteria.title).map(data => Book.fromJSON(data))
    }

    getById(id: Book['id']) {
        const data = this.books.find(b => b.id === id)
        return data ? Book.fromJSON(data) : undefined
    }

    create(payload: CreateBookPayload) {
        return Book.create(payload)
    }

    update(id: Book['id'], payload: UpdateBookPayload) {
        const book = this.getById(id)
        if (!book) { throw new Error('Book not found') }
        return book.update(payload)
    }
}

const bookLibrary = new BookRepository(books)
