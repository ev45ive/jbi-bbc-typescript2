
// tsrafc
import axios, { AxiosError } from 'axios'
import React, { useEffect, useState } from 'react'
import { SearchBox } from '../components/SearchBox'
import { BookData, BooksAPIErrorResponse, BooksResponse } from '../model/Books'

interface Props {

}

const booksApiKey = 'AIzaSyCMbicbDiZyHWpRMr0r2qHqmvQ9wApulCo'

export const BookSearch = (props: Props) => {
    const [results, setResults] = useState<BookData[] | null>(null)
    const [message, setMessage] = useState('')
    const [currentQuery, setCurrentQuery] = useState<string | null>(null)

    useEffect(() => {
        if (!currentQuery) { return }

        (async () => {
            try {

                const { data } = await axios.get<BooksResponse>(`https://www.googleapis.com/books/v1/volumes`, {
                    params: {
                        q: currentQuery,
                        maxResults: 40,
                        key: booksApiKey
                    }
                })
                // if(isValidBooksResponse){ ..
                setResults(data.items)
            } catch (error) {
                if (isBooksErrorResponse(error)) {
                    setMessage(error.response!.data.error.message)
                } else {
                    setMessage('Unknown Error')
                }
            }
        })()

        return () => { }

    }, [currentQuery])

    return (
        <div>
            {/* .row*2>.col */}
            <div className="row">
                <div className="col">
                    <SearchBox onSearch={setCurrentQuery} />
                </div>
            </div>
            <div className="row">
                <div className="col">

                    {message && <p className="alert alert-danger">{message}</p>}
                    {!results && !message && <p className="alert alert-info">No results</p>}

                    <div className="row row-cols-1 row-cols-sm-4 no-gutters">
                        {results && results.map(result => <div className="col mb-4" key={result.id}>
                            <div className="card">
                                {result.volumeInfo.imageLinks && <img src={result.volumeInfo.imageLinks?.small} className="card-img-top" alt="..." />}
                                <div className="card-body">
                                    <h5 className="card-title">{result.volumeInfo.title}</h5>
                                </div>
                            </div>
                        </div>)}

                    </div>
                </div>
            </div>
        </div>
    )
}

export function isErrorResponse(error: any): error is AxiosError {
    return ('isAxiosError' in error)
}

export function isBooksErrorResponse(error: any): error is AxiosError<BooksAPIErrorResponse> {
    if (isErrorResponse(error)) {
        return error.response?.data?.error?.message
    } else { return false }
}