"use strict";

function add(a?:string, b?:string) {
    if (!a || !b) {
        throw new Error('Only strings');
    }
    return a + b;
}

// const result2 = add('1', '2')
var result = '';
result = add(undefined, undefined);
result = add();
result = add('Alice and', ' a cat');
console.log(result);
